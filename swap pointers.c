#include<stdio.h>
void swap(int *num1,int *num2)
{
int temp;
temp=*num1;
*num1=*num2;
*num2=temp;
}
int main()
{
int *num1,*num2;
printf("enter two numbers to be swapped\n");
printf("enter num1:\n");
scanf("%d",&num1);
printf("enter num2:\n");
scanf("%d",&num2);
swap(&num1,&num2);
printf("after swapping num1:%d num2:%d\n",num1,num2);
return 0;
}